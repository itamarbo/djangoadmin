from django.db import models
from django.db.models.fields.related import ManyToManyField

class App (models.Model):
    App_Family = models.CharField(max_length=50)
    App_Name = models.CharField(max_length=100)
    App_Description = models.TextField(blank=True, null=True)
    App_Category = models.CharField(max_length=100)
    SV_Available = models.TextField(blank=True, null=True)
    Notes_And_Comments = models.TextField(blank=True, null=True)
    SV_Value_Proposition = models.TextField(blank=True, null=True)
    App_Supported_ODD = models.TextField(blank=True, null=True)
    Required_Sensors = models.TextField(blank=True, null=True)
    App_Output = models.TextField(blank=True, null=True)
    App_KPI_Overview = models.TextField(blank=True, null=True)
    Additional_Information = models.TextField(blank=True, null=True)

    


class Product(models.Model):
    Product_Name = models.CharField(max_length=100)
    AppID = models.IntegerField(blank=True, null=True)
    Type = models.CharField(max_length=100, blank=True, null=True)

    
    


class ProductML (models.Model):
    Product_id = models.IntegerField()
    Mono_ML4_Target_Bundle = models.CharField(max_length=1000, blank=True, null=True)
    Wono_ML4_Target_Bundle = models.CharField(max_length=1000, blank=True, null=True)
    EightMono_ML4_Target_Bundle = models.CharField(max_length=1000, blank=True, null=True)
    Mono_ML5_Target_Bundle = models.CharField(max_length=1000, blank=True, null=True)
    Wono_ML5_Target_Bundle = models.CharField(max_length=1000, blank=True, null=True)
    EightMono_ML5_Target_Bundle = models.CharField(max_length=1000, blank=True, null=True)
    Mono_Maturity = models.CharField(max_length=1000, blank=True, null=True)
    Wono_Maturity = models.CharField(max_length=1000, blank=True, null=True)
    EightMono_Maturity = models.CharField(max_length=1000, blank=True, null=True)

    

            
class ProductInfo (models.Model):
    Product_id = models.IntegerField()
    HL_Signals = models.TextField(blank=True, null=True)
    Notes = models.TextField(blank=True, null=True)
    Product_Family = models.TextField(blank=True, null=True)
    Product_Sales_Tier = models.TextField(blank=True, null=True)
    Mono_Support = models.TextField(blank=True, null=True)
    Wono_Support = models.TextField(blank=True, null=True)
    Eight_Mono_Support = models.TextField(blank=True, null=True)
    Product_Description = models.TextField(blank=True, null=True)
    FULL_Value_Proposition = models.TextField(blank=True, null=True)
    Biz_Objectives = models.TextField(blank=True, null=True)
    Descriptive = models.TextField(blank=True, null=True)
    FRP = models.TextField(blank=True, null=True)
    Requirements = models.TextField(blank=True, null=True)
    Mandatory_For_Apps = models.TextField(blank=True, null=True)
    Optional_For_Apps = models.TextField(blank=True, null=True)
    EE_To_ML5 = models.TextField(blank=True, null=True)

   