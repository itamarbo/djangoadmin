from django.apps import AppConfig


class MeApplicationsProductsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ME_Applications_Products'
