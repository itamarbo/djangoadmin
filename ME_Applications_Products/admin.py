from django.contrib import admin
from . import models
from import_export.admin import ImportExportModelAdmin
from  .models import App, Product, ProductML, ProductInfo

class AppAdminArea(admin.AdminSite):
    site_header = 'App Admin Area'
    login_template = 'admin/login.html'
    

admin_site = AppAdminArea(name='AppAdmin')

#admin.site.register(models.post)

@admin.register(App)
class postAdmin(ImportExportModelAdmin):
    list_display = ("App_Name", "App_Family","App_Description")
    list_filter = ("App_Name", "App_Family","App_Category", "SV_Available","App_Output")
    pass


@admin.register(Product)
class postAdmin(ImportExportModelAdmin):
    list_display = ("Product_Name", "AppID", "Type")
    list_filter = ( "AppID", "Type", "Product_Name")
    pass

@admin.register(ProductML)
class postAdmin(ImportExportModelAdmin):
    list_display = ("Mono_ML4_Target_Bundle", "Wono_ML4_Target_Bundle", "EightMono_ML4_Target_Bundle",
                    "Mono_ML5_Target_Bundle", "Wono_ML5_Target_Bundle", "EightMono_ML5_Target_Bundle", "Mono_Maturity",
                    "Wono_Maturity", "EightMono_Maturity"
                    )
    list_filter = ("Mono_ML4_Target_Bundle", "Wono_ML4_Target_Bundle", "EightMono_ML4_Target_Bundle",
                    "Mono_ML5_Target_Bundle", "Wono_ML5_Target_Bundle", "EightMono_ML5_Target_Bundle", "Mono_Maturity",
                    "Wono_Maturity", "EightMono_Maturity"
                    )               
    pass

@admin.register(ProductInfo)
class postAdmin(ImportExportModelAdmin):
    list_display = ("Product_id", "Product_Family", "HL_Signals", "Product_Sales_Tier", "Notes", "Mono_Support", "Wono_Support", "Eight_Mono_Support")
    list_filter = ("Product_Family", "HL_Signals", "Product_Sales_Tier", "Mono_Support", "Wono_Support", "Eight_Mono_Support")
    pass



   