from django.contrib import admin
from django.urls import path

from blog.admin import blog_site
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', admin.site.urls),
 #   path('admin/', admin.site.urls),
    
    path('blogadmin/', blog_site.urls)
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)



admin.site.site_header = "BI Team System"
admin.site.index_title = "Data Dashboard"
admin.site.site_title = "BI Team System For Admin"
