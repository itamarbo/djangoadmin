from django.contrib import admin
from . import models
from import_export.admin import ImportExportModelAdmin
from  .models import post
#from  book.models import books
#from  book.admin import book_site
#import book
class BlogAdminArea(admin.AdminSite):
    site_header = 'Blog Admin Area'
    login_template = 'admin/login.html'

blog_site = BlogAdminArea(name='BlogAdmin')    

#admin.site.register(models.post)
blog_site.register(models.post)
#book_site.register(book.models.books)

@admin.register(post)
class postAdmin(ImportExportModelAdmin):
    pass